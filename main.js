document.addEventListener("DOMContentLoaded", function () {
    // Cache DOM elements
    const tbody = document.querySelector("table tbody");
    const scheduleWrap = document.querySelector(".schedule-wrap");

    // Define ifter and sehri time
    const calendar = {
        "2024-03-11": { ifter: "6:09 PM", sehri: "04:52 AM" },
        "2024-03-12": { ifter: "6:10 PM", sehri: "04:51 AM" },
        "2024-03-13": { ifter: "6:10 PM", sehri: "04:50 AM" },
        "2024-03-14": { ifter: "6:11 PM", sehri: "04:49 AM" },
        "2024-03-15": { ifter: "6:11 PM", sehri: "04:48 AM" },
        "2024-03-16": { ifter: "6:12 PM", sehri: "04:47 AM" },
        "2024-03-17": { ifter: "6:12 PM", sehri: "04:46 AM" },
        "2024-03-18": { ifter: "6:12 PM", sehri: "04:45 AM" },
        "2024-03-19": { ifter: "6:13 PM", sehri: "04:44 AM" },
        "2024-03-20": { ifter: "6:13 PM", sehri: "04:43 AM" },
        "2024-03-21": { ifter: "6:13 PM", sehri: "04:42 AM" },
        "2024-03-22": { ifter: "6:14 PM", sehri: "04:41 AM" },
        "2024-03-23": { ifter: "6:14 PM", sehri: "04:40 AM" },
        "2024-03-24": { ifter: "6:14 PM", sehri: "04:39 AM" },
        "2024-03-25": { ifter: "6:15 PM", sehri: "04:38 AM" },
        "2024-03-26": { ifter: "6:15 PM", sehri: "04:36 AM" },
        "2024-03-27": { ifter: "6:16 PM", sehri: "04:35 AM" },
        "2024-03-28": { ifter: "6:16 PM", sehri: "04:34 AM" },
        "2024-03-29": { ifter: "6:17 PM", sehri: "04:33 AM" },
        "2024-03-30": { ifter: "6:17 PM", sehri: "04:31 AM" },
        "2024-03-31": { ifter: "6:18 PM", sehri: "04:30 AM" },
        "2024-04-01": { ifter: "6:18 PM", sehri: "04:29 AM" },
        "2024-04-02": { ifter: "6:19 PM", sehri: "04:28 AM" },
        "2024-04-03": { ifter: "6:19 PM", sehri: "04:27 AM" },
        "2024-04-04": { ifter: "6:19 PM", sehri: "04:26 AM" },
        "2024-04-05": { ifter: "6:20 PM", sehri: "04:24 AM" },
        "2024-04-06": { ifter: "6:20 PM", sehri: "04:24 AM" },
        "2024-04-07": { ifter: "6:21 PM", sehri: "04:23 AM" },
        "2024-04-08": { ifter: "6:21 PM", sehri: "04:22 AM" },
        "2024-04-09": { ifter: "6:21 PM", sehri: "04:21 AM" },
        "2024-04-10": { ifter: "6:22 PM", sehri: "04:20 AM"},
    };

    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = (currentDate.getMonth() + 1).toString().padStart(2, "0");
    const day = currentDate.getDate().toString().padStart(2, "0");
    const today = `${year}-${month}-${day}`;

    // Format date function
    const formatDate = (dateString) => {
        const [year, month, day] = dateString.split("-");
        const monthNames = [
        "JAN", "FEB", "MAR", "APR", "MAY", "JUN", 
        "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
        ];
        return `${parseInt(day)} ${monthNames[parseInt(month) - 1]}, ${year}`;
    };

    // Generate HTML for current date
    const { ifter, sehri } = calendar[today] || { ifter: "-", sehri: "-" };
    const date = formatDate(today);
    const html = `<tr><td>${date}</td><td>${sehri}</td><td>${ifter}</td></tr>`;

    // Update DOM with generated HTML
    tbody.innerHTML = html;

    // Open donation link on schedule wrap click
    scheduleWrap.addEventListener("click", function () {
        window.open("https://donation.bkash.com/en/mastul/donate", "_blank");
    });
});